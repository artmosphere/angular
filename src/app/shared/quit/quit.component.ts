import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quit',
  templateUrl: './quit.component.html',
  styleUrls: ['./quit.component.scss']
})
export class QuitComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window['ipc'].send('quit-application');
  }

}
